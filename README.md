# Welcome to the ctf-game!! 

Welcome to the best ctf in bsmch!!!
**CTF**
In each level, you need to use shell commands to find the clue that transfers you to the next level, using only your virtual machines.

**The clue for each level will always be inside the README.md file.**

### Let's get this journey started!! 

``Level 0``

The password for the first level stored in a file called **start** located in the root folder of ther repo.

> useful commands: ls, cd, cat, file, find.
